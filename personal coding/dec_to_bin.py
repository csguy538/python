conversion_table = ["0", "1"]

dec = int(input("Enter a number: "))
bin = " "

while(dec > 0):
    remainder = dec % 2
    bin = conversion_table[remainder] + bin
    dec = dec // 2
print("Binary: ", bin)