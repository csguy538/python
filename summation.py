#!/usr/bin/env python3

n = int(input("Input an integer : "))
sum = 0 
print("n | sum")
for i in range(1, n+1):
	sum = sum + i
	print(i, sum)
	
print("The summation of " + str(n) + " is " + str(sum))

# factorial is above; change sum to equal 0 and sum + i to go back to summation
