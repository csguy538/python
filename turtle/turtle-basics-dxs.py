import turtle

screen = turtle.Screen()
t = turtle.Turtle()
turtle.tracer(1, 0)
t.speed(0)
t.width(1)
t.ht()

go = .1
t.fillcolor("green")
t.begin_fill()
for i in range(2560):
    t.fd(go)
    t.lt(5)
    go = go + 0.01
t.lt(90)
t.fd(8.5)
t.end_fill()
screen.exitonclick()	

if __name__ == "__main__":
	main()