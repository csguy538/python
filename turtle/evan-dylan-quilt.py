import turtle
import random
turtle.colormode(255)
screen = turtle.Screen()
min_branch_len = 5

def evan():
	def buildtree(t, branch_length, shorten_by, angle):
		if branch_length > min_branch_len:
			t.fd(branch_length)
			new_length = branch_length - shorten_by

			t.lt(angle)
			buildtree(t, new_length, shorten_by, angle)

			t.rt(angle * 2)
			buildtree(t, new_length, shorten_by, angle)

			t.lt(angle)
			t.backward(branch_length)
	def buildTurtleTrees():
		turtle.bgcolor("#c2c2c2")
		tree.speed(10000)
		tree.color("#ca0000")
		tree.ht()	
		turtle.tracer(1,0)
		tree.fd(175)
		buildtree(tree, 50, 5, 30)
		turtle.penup
		tree.goto(0, 0)
		turtle.pendown
		tree.lt(90)
		tree.color("#54a630")
		buildtree(tree, 50, 5, 30)
		turtle.penup
		tree.goto(0,0)
		turtle.pendown
		tree.lt(180)
		tree.color("blue")
		buildtree(tree, 50, 5, 30)
		turtle.penup
		tree.goto(0,0)
		turtle.pendown
		tree.lt(270)
		tree.color("yellow")
		tree.fd(175)
		buildtree(tree, 50, 5, 30)
	buildTurtleTrees()

def dylan():
	def lineOne():
		for i in range(1000):
			t = turtle.Turtle()
			R = random.randint(0, 255)
			G = random.randint(0, 255)
			B = random.randint(0, 255)
			t.color(R, G, B)
			t.width(2)
			t.ht()
			t.penup()
			t.goto(-40, 275)
			t.pendown()
			t.fd(80)
			t.rt(4)
			R = random.randint(0, 255)
			G = random.randint(0, 255)
			B = random.randint(0, 255)
			t.color(R, G, B)
			for i in range(92):
				t.fd(2)
				t.rt(.55)
			t.lt(85)
			R = random.randint(0, 255)
			G = random.randint(0, 255)
			B = random.randint(0, 255)
			t.color(R, G, B)
			for i in range(345):
				t.fd(1)
				t.rt(.35)
			R = random.randint(0, 255)
			G = random.randint(0, 255)
			B = random.randint(0, 255)
			t.color(R, G, B)
			t.goto(442, -40)
			R = random.randint(0, 255)
			G = random.randint(0, 255)
			B = random.randint(0, 255)
			t.color(R, G, B)
			for i in range(345):
				t.fd(1)
				t.rt(.35)
			t.lt(88.5)
			R = random.randint(0, 255)
			G = random.randint(0, 255)
			B = random.randint(0, 255)
			t.color(R, G, B)
			for i in range(92):
				t.fd(2)
				t.rt(.55)
			R = random.randint(0, 255)
			G = random.randint(0, 255)
			B = random.randint(0, 255)
			t.color(R, G, B)
			t.goto(40, -275)
			t.goto(-40, -275)
			t.rt(10)
			R = random.randint(0, 255)
			G = random.randint(0, 255)
			B = random.randint(0, 255)
			t.color(R, G, B)
			for i in range(87):
				t.fd(2)
				t.rt(.55)
			t.lt(83)
			R = random.randint(0, 255)
			G = random.randint(0, 255)
			B = random.randint(0, 255)
			t.color(R, G, B)
			for i in range(346):
				t.fd(1)
				t.rt(.35)
			R = random.randint(0, 255)
			G = random.randint(0, 255)
			B = random.randint(0, 255)
			t.color(R, G, B)
			t.goto(-442, -40)
			t.goto(-442, 40)
			R = random.randint(0, 255)
			G = random.randint(0, 255)
			B = random.randint(0, 255)
			t.color(R, G, B)
			for i in range(345):
				t.fd(1)
				t.rt(.35)
			t.lt(85.25)
			R = random.randint(0, 255)
			G = random.randint(0, 255)
			B = random.randint(0, 255)
			t.color(R, G, B)
			for i in range(94):
				t.fd(2)
				t.rt(.55)
			R = random.randint(0, 255)
			G = random.randint(0, 255)
			B = random.randint(0, 255)
			t.color(R, G, B)
	lineOne()

#***********MAIN************
def main():
	turtle.tracer(1,0)
	evan()
	dylan()

tree = turtle.Turtle()
main()

screen.exitonclick()	

if __name__ == "__main__":
	main()